<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>OscarMZ</title>
  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">mobver <sup>-test</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="index.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Inicio</span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="https://gitlab.com/oscarazul97/mobver-test.git">
        <i class="far fa-user"></i>
          <span>Proyecto GitLab </span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="https://www.linkedin.com/in/oscar-manriquez-zavala-bb15a916b/">
        
        <i class="fab fa-linkedin-in"></i>
          <span>Linkedin </span></a>
      </li>
      <li class="nav-item active">
      <a class="nav-link" href="https://www.facebook.com/manriquezzavala">
        <i class="fab fa-facebook-square"></i>
          <span>Facebook </span></a>
      </li>
      <li class="nav-item active">
      <a class="nav-link" href="https://blackrockdigital.github.io/startbootstrap-sb-admin-2/">
      <i class="fab fa-linux"></i>
          <span>Plantilla Utilizada </span></a>
      </li>
      <li class="nav-item active">
      <a class="nav-link" href="https://docs.google.com/document/d/1-nDuLweWeBM2Kk9mvp26yQKZKnIU_snY3NIX_-pKsoQ/edit?usp=sharing">
      <i class="fas fa-file-word"></i>
          <span>Documentación Del Programa </span></a>
      </li>
      
      <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Divider -->
      <hr class="sidebar-divider d-block d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-block d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4" >
            <h1 class="h3 mb-0 text-gray-800">ITM-Ponys->Oscar Manriquez Zavala</h1>
          </div>

          <!-- Content Row -->
          <div class="row">
        
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-md-12 col-xs-12">
            <form id="formulariobase" name="formulariobase" method="post">
              <div class="form-row">
                <div class="col-md-4 col-xs-12">
                  <label for="validationDefaultUsername">Valor Flotante</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroupPrepend2">%</span>
                    </div>
                    <input id="valorFlotante" name="valorFlotante" type="number" class="form-control" min="0.0" max="100" step="0.1"  placeholder="" aria-describedby="inputGroupPrepend2" required>
                  </div>
                </div>
                <div class="col-md-4 col-xs-12">
                  <label for="validationDefaultUsername">Texto</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroupPrepend2"><svg class="bi bi-card-text" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd" d="M14.5 3h-13a.5.5 0 00-.5.5v9a.5.5 0 00.5.5h13a.5.5 0 00.5-.5v-9a.5.5 0 00-.5-.5zm-13-1A1.5 1.5 0 000 3.5v9A1.5 1.5 0 001.5 14h13a1.5 1.5 0 001.5-1.5v-9A1.5 1.5 0 0014.5 2h-13z" clip-rule="evenodd"/>
                      <path fill-rule="evenodd" d="M3 5.5a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9a.5.5 0 01-.5-.5zM3 8a.5.5 0 01.5-.5h9a.5.5 0 010 1h-9A.5.5 0 013 8zm0 2.5a.5.5 0 01.5-.5h6a.5.5 0 010 1h-6a.5.5 0 01-.5-.5z" clip-rule="evenodd"/>
                      </svg>
                      </span>
                    </div>
                    <input id="valorTexto" name="valorTexto" type="text" class="form-control"  placeholder="" aria-describedby="inputGroupPrepend2" required>
                  </div>
                </div>
                <div class="col-md-4 col-xs-12">
                  <label for="validationDefaultUsername">Entero 1 al 1000</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroupPrepend2">N</span>
                    </div>
                    <input id="valorEntero" name="valorEntero" type="number" min="1" max="1000" class="form-control"  placeholder="" aria-describedby="inputGroupPrepend2" required>
                  </div>
                </div>
                <div class="col-md-4 offset-md-4 col-xs-12" style="margin-top:1%;">
                <input type="file" id="fileinput" name="fileinput" required>
                </div>
               <div class="col-md-6  col-xs-12" style="margin-top:1%;">
                <button type="summit" id="btnForm" name="btnForm" style="width: 100%;"  class="btn btn-outline-success">Enviar ó (Se Recomienda Ejecutar Prueba)</button>
              </div>
              <div class="col-md-6  col-xs-12" style="margin-top:1%;">
                <button type="button" id="btnPrueba" onclick="IniciarPrueba()" name="btnPrueba" style="width: 100%;"  class="btn btn-outline-info">Ejecutar Prueba (Se llenan los campos en automatico)</button>
              </div>
              </div>
            </form>
            </div>
            <div class="col-md-4 offset-md-4 col-xs-12 text-center" style="margin-top:1%;">
                 <div id="imgCargaImagen"></div>
            </div>
          </div>
          <!-- OscarMZ -->
          <div class="row" style="margin-top:1%;">
        
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Parrafos Encontrados</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><label id="labelparrafo"></label></div>
                    </div>
                    <div class="col-auto">
                    <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Palabras Encontradas</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><label id="labelpalabras"></label></div>
                    </div>
                    <div class="col-auto">
                    <i class="fas fa-comments fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Pending Requests Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Lineas</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><label id="labellineas"></label></div>
                    </div>
                    <div class="col-auto">
                    <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Content Row -->
          <div class="row">
            <!-- Area Chart -->
            <!-- Pie Chart -->
            <div class="col-md-5 col-xs-12">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Top 3</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-pie pt-4 pb-2">
                    <canvas id="myPieChart"></canvas>
                  </div>
                  <div class="mt-4 text-center small">
                    <span class="mr-2">
                      <i class="fas fa-circle text-primary"></i>
                       <label id=top1label></label>
                    </span>
                    <span class="mr-2">
                      <i class="fas fa-circle text-success"></i>
                      <label id=top2label></label>
                    </span>
                    <span class="mr-2">
                      <i class="fas fa-circle text-info"></i>
                      <label id=top3label></label>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-7 col-xs-12" >

              <!-- Project Card Example -->
              <div class="card shadow mb-4" style="height: 413px;">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Top Documento</h6>
                </div>
                <div class="card-body">
                  <h4 class="small font-weight-bold"><label id="topname0"></label> <span class="float-right"><label id="topporcentaje0"></label></span></h4>
                  <div class="progress mb-4">
                    <div class="progress-bar bg-danger" id="porcentajeTop0" role="progressbar" style="width: 0%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <h4 class="small font-weight-bold"><label id="topname1"></label> <span class="float-right"><label id="topporcentaje1"></label></span></h4>
                  <div class="progress mb-4">
                    <div class="progress-bar bg-warning" id="porcentajeTop1" role="progressbar" style="width: 0%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <h4 class="small font-weight-bold"><label id="topname2"></label> <span class="float-right"><label id="topporcentaje2"></label></span></h4>
                  <div class="progress mb-4">
                    <div class="progress-bar" id="porcentajeTop2" role="progressbar" style="width: 0%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <h4 class="small font-weight-bold"><label id="topname3"></label> <span class="float-right"><label id="topporcentaje3"></label></span></h4>
                  <div class="progress mb-4">
                    <div class="progress-bar bg-info" id="porcentajeTop3" role="progressbar" style="width: 0%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <h4 class="small font-weight-bold"><label id="topname4"></label> <span class="float-right"><label id="topporcentaje4"></label></span></h4>
                  <div class="progress">
                    <div class="progress-bar bg-success" id="porcentajeTop4" role="progressbar" style="width: 0%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>

              <!-- Color System -->

            </div>
            <div class="col-md-12 col-xs-12">
              <div class="card shadow mb-4">
                 <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Información del archivo</h6>
                 </div>
                 <div class="card-body" id="CargarTableData">
                 </div>
              </div>
            </div>
            <div class="col-md-12 col-xs-12">
              <div class="card shadow mb-4">
                 <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Posible Párrafo</h6>
                 </div>
                 <div class="card-body" id="CargarTableSentencia">
                 </div>
              </div>
            </div>
            <div class="col-md-12 col-xs-12">
              <div class="card shadow mb-4">
                 <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Información Bigram</h6>
                 </div>
                 <div class="card-body" id="CargarTableDataBigram">
                 </div>
              </div>
            </div>
            
          </div>
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-pie-demo.js"></script>

  <!-- Page level custom scripts -->
  <script src="algoritmos.js"></script>
  <script src="ControlPrincipal.js"></script>
</body>
</html>
