$(document).on("submit", "#formulariobase", function(e) {
  e.preventDefault();
  $("#imgCargaImagen").html("<div> <img src=\"img/loader.gif\" alt=\"Cargando\" style=\"width: 20%\" > </div>");
  CargarArchivo();
  //Carga de archivo se guarda posteriormente se lee
});

function Inicializar(formData,nombre){
  formData.delete('funcion');
  formData.append("funcion", "LeerArchivoPhP");
  formData.append("nombre", nombre);
  $.ajax({
    url: "funciones.php",
    type: "post",
    dataType: "html",
    data: formData,
    cache: false,
    contentType: false,
    processData: false
})
    .done(function(respuesta){
      //tenemos una respuesta la cual nos retorna un objeto con datos procesados 
      //Se empieza a cargar los datos con el objeto
       var objeto = algoritmo_dobleSalto(respuesta);
       CargarCirculo(objeto);//Codigo De la plantilla Reutilizado con nuevos valores procesados
       topDocumento(objeto);//Codigo De la plantilla Reutilizado con nuevos valores procesados
       tarjetas(objeto);//Codigo De la plantilla Reutilizado con nuevos valores procesados
       CargarInfoTable(objeto,$("#valorFlotante").val(),$("#valorTexto").val());
       //se manda un ajax con el informacion para el retorno de codigo html para incluir 
       CargarInfoTablebigram(objeto); 
       //se manda un ajax con el informacion para el retorno de codigo html para incluir 
       
    });
}

function CargarArchivo(){
   var formDataArchivo = new FormData(document.getElementById("formulariobase"));
   formDataArchivo.append("funcion", "SubirArchivoPhP");
   $.ajax({
     url: "funciones.php",
     type: "post",
     dataType: "html",
     data: formDataArchivo,
     cache: false,
     contentType: false,
     processData: false
    })
     .done(function(respuesta){
       Inicializar(formDataArchivo,'archivo.txt');
       //despues de Carga de archivo Inicializa la carga del contenido
       //se envia archivo.txt es el nombre que se asigna al  gurdar el archivo
       //para ser remplazado posteriormente con más archivos
     });
}

function CargarCirculo(objeto){
  //Código Reutilizado con calculo de nuevos porcentajes
var maxporcentaje = objeto.Top1+objeto.Top2+objeto.Top3;
var porcentajetop1 = ((objeto.Top1*100)/maxporcentaje).toFixed(2);
var porcentajetop2 = ((objeto.Top2*100)/maxporcentaje).toFixed(2);
var porcentajetop3 = ((objeto.Top3*100)/maxporcentaje).toFixed(2);
$("#top1label").text(objeto.Top1W+' '+porcentajetop1+'%');
$("#top2label").text(objeto.Top2W+' '+porcentajetop2+'%');
$("#top3label").text(objeto.Top3W+' '+porcentajetop3+'%');
// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

// Pie Chart Example
var ctx = document.getElementById("myPieChart");
var myPieChart = new Chart(ctx, {
type: 'doughnut',
data: {
  labels: [objeto.Top1W, objeto.Top2W, objeto.Top3W],
  datasets: [{
    data: [porcentajetop1, porcentajetop2, porcentajetop3],
    backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc'],
    hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf'],
    hoverBorderColor: "rgba(234, 236, 244, 1)",
  }],
},
options: {
  maintainAspectRatio: false,
  tooltips: {
    backgroundColor: "rgb(255,255,255)",
    bodyFontColor: "#858796",
    borderColor: '#dddfeb',
    borderWidth: 1,
    xPadding: 15,
    yPadding: 15,
    displayColors: false,
    caretPadding: 10,
  },
  legend: {
    display: false
  },
  cutoutPercentage: 80,
},
});

}

function CargarInfoTable(objecto,flotant,palabralibre){
  var sendpost = JSON.stringify(objecto);
  $.ajax({
      url: "funciones.php",
      type: "post",
      dataType: "html",
      data: {funcion: "CargarTabla", obj: sendpost, valorflot: flotant,txtlibre: palabralibre},
  })
      .done(function(respuesta){
          $("#CargarTableData").html(respuesta);
          var table = $('#dataTable').DataTable({
              "aaSorting": [[ 1, "desc" ]]
            });
            $("#imgCargaImagen").html('');

      });
}

function CargarInfoTablebigram(objecto){
  var sendpost = JSON.stringify(objecto);
  $.ajax({
      url: "funciones.php",
      type: "post",
      dataType: "html",
      data: {funcion: "CargarTablaBigram", obj: sendpost},
  })
      .done(function(respuesta){
          $("#CargarTableDataBigram").html(respuesta);
          CargarInfoTableSentencias(objecto);

      });
}
function CargarInfoTableSentencias(objecto){
  var sendpost = JSON.stringify(objecto);
  console.log('sendpost');
  console.log(sendpost);
  $.ajax({
      url: "funciones.php",
      type: "post",
      dataType: "html",
      data: {funcion: "CargarPosiblesSentencias", obje2: sendpost, objpalabras: $("#jsonPalabrasPermitidas").val()},
  })
      .done(function(respuesta){
          $("#CargarTableSentencia").html(respuesta);
          $("#imgCargaImagen").html('');
      });
}

function topDocumento(objeto){
  var Top5 = [objeto.Top1, objeto.Top2,objeto.Top3,objeto.Top4,objeto.Top5];
  var word = [objeto.Top1W, objeto.Top2W,objeto.Top3W,objeto.Top4W,objeto.Top5W];
  var i = 0;
  Top5.forEach(function(top) {
     
      //  asignacion de texto
      var porcentajetop = ((top*100)/objeto.TotalW).toFixed(2);
      var barra = document.getElementById('porcentajeTop'+i);
      barra.style.width= porcentajetop+'%';
      $("#topname"+i).text(word[i]);
      $("#topporcentaje"+i).text(porcentajetop+'%');
      i++;
  });

}
function tarjetas(objeto){
  //  asignacion de texto
  if(objeto.parrafos == 0){
    $("#labelparrafo").text('Error');
  }
  else
   $("#labelparrafo").text(objeto.parrafos);
  
  $("#labelpalabras").text(objeto.TotalW);
  $("#labellineas").text(objeto.Tlinea);
}
function IniciarPrueba(){
  //  Se carga la prueba por defecto
  $("#imgCargaImagen").html("<div> <img src=\"img/loader.gif\" alt=\"Cargando\" style=\"width: 20%\" > </div>");
  $("#valorFlotante").val('0.5');
  $("#valorTexto").val('and');
  var formDataArchivo = new FormData(document.getElementById("formulariobase"));
  Inicializar(formDataArchivo,'prueba.txt');
}
//valorFlotante valorTexto valorEntero
//labelparrafo labelpalabras labellineas
