    var arrayP = new Array();
    var arraybigram = new Array();
    /*--------------------- algoritmo_dobleSalto --------------------- */
     /* Se basa en encontrar dos saltos de lineas seguidos puede fallar cuando hay más de 2 saltos \n 
     ya no alcance a corregir el error ... Fallaria el conteo de parrafos y posiblemente el bigram.
     Se recomienda Usar ejecutar el boton de prueba. se cargaria el mismo archivo enviado bigfile.txt
     PD:Solo se modifico un Salto de linea. por el mismo problema
     
     Desarrolle un segundo algoritmo "algoritmo_MayusculaPuntoYaparte"
     y pude encontrar que en el archivo bigfile.txt el parrafo 135, y 28 no se reconocia el parrafo 
     puesto que no terminaba en punto final
     */
    function algoritmo_dobleSalto(respuesta){
        arrayP = new Array();
        arraybigram = new Array();
        var Lineas = JSON.parse(respuesta);
        var ssd='';
        var parrafos = 0;
        var LineaAnterior2 ='';
        for (var i = 0; i < Lineas.length; i++) {
            if(i+1<Lineas.length){
                var res = Lineas[i]['linea'].replace(/ /gi, "");    
                if(res == '\n' && LineaAnterior2 == '\n'){
                    parrafos++;
                    console.log((i+1)+' - '+Lineas[i]['linea']);
                }
            }
            LineaAnterior2 = res;
            PalabrasRepetidas(Lineas[i]['linea'],(parrafos));
         }
         var objecto = TopPalabras();
         objecto.parrafos = parrafos;
         objecto.Tlinea = Lineas.length;
         objecto.dataPalabras = arrayP;
         objecto.bigram = arraybigram;
         console.log(objecto);
         //Retorna un objeto con toda la informacion procesada
        return objecto;
    }
    function palabrasEncontrada(linea){
    if(linea.includes('\n')==true){
        linea = linea.replace(/\n/gi, "");   
    }
    linea = linea.trim();
    palabrasporlinea = linea.split(' ');
    return palabrasporlinea;
    }
    function PalabrasRepetidas(linea,parrafo){
    var word = palabrasEncontrada(linea);
    var i=0;
    var anteriosbigram='';
    while (i<word.length){
        var mayus = word[i].toUpperCase();
        if(i-1 >=0){
            anteriosbigram= word[i-1].toUpperCase();
        }
        else{
            anteriosbigram = '';
        }
        if(PalabrasRepetidasArray(arrayP,mayus,parrafo,anteriosbigram) == true){
            var objeto = new Object();
            objeto.palabra = mayus;
            objeto.repetidos = 1;
            objeto.parrafo = parrafo;
            arrayP.push(objeto);
        }     
        i++;
    }
    
    }
    function PalabrasRepetidasArray(Words,Nueva,parrafo,anterior){
    //Se encarga de hacer un array de objetos y asignar palabras no existentes en el array
    //de tal forma que solo este una palabra con una llave para ver sus repeticiones(objeto). 
    //Se Crea un objeto para insertar en el array de objetos para crear el Bigram de Todo el documento
    var bandera = true;
    var bigram = new Object();
    if(Words.length == 0){
        bigram.palabra = Nueva;
        bigram.anterior = anterior;
        arraybigram.push(bigram);
        return true;
    }
    if(Esnumero(Nueva) == false || Words == ""){return false;}
    Words.forEach(function(objeto) {
        if(objeto.palabra == Nueva){
            var suma = objeto.repetidos;
            suma = suma+1;
            objeto.repetidos = suma;
            objeto.parrafo = objeto.parrafo+'\n'+parrafo;
            bandera = false;
            bigram.palabra = Nueva;
            bigram.anterior = anterior;
            arraybigram.push(bigram);
        }
    });
    return bandera;
    }
    function Esnumero(linea){
        //evita sumar palabras por numeros
    try {
        parseInt(linea);
      }
      catch(error) {
        console.error(error);
        return false;
      }  
      return true
    }
    function TopPalabras(){
        /*--------------------- Se determina el Top de todas las palabras --------------------- */
        /*--------------------- Tambien el total de palabras encontradas --------------------- */
    var top1=0, top2=0, top3=0, top4=0, top5=0;
    var palabratop1='', palabratop2='', palabratop3='', palabratop4='', palabratop5='';
    var TotalPalabras = 0;
    arrayP.forEach(function(obj) {
        TotalPalabras = TotalPalabras+obj.repetidos;
        if(obj.repetidos > top1){
            top5 = top4; palabratop5 = palabratop4;
            top4 = top3; palabratop4 = palabratop3;
            top3 = top2; palabratop3 = palabratop2;
            top2 = top1; palabratop2 = palabratop1;
            top1 = obj.repetidos; palabratop1 = obj.palabra;
        }
        else{
            if(obj.repetidos > top2){
                top5 = top4;  palabratop5 = palabratop4;
                top4 = top3;  palabratop4 = palabratop3;
                top3 = top2;  palabratop3 = palabratop2;
                top2 = obj.repetidos; palabratop2 = obj.palabra;
            }
            else{
                if(obj.repetidos > top3){
                    top5 = top4; palabratop5 = palabratop4;
                    top4 = top3; palabratop4 = palabratop3;
                    top3 = obj.repetidos; palabratop3 = obj.palabra;
                }
                else{
                    if(obj.repetidos > top4){
                        top5 = top4; palabratop5 = palabratop4;
                        top4= obj.repetidos;  palabratop4 = obj.palabra;
                    }
                    else{
                        if(obj.repetidos > top5){
                            top5= obj.repetidos;
                            palabratop5 = obj.palabra;
                        }
                    }
                }
            }
            
        }
    });
       var datosPalabras = new Object();
        datosPalabras.Top1 = top1;
        datosPalabras.Top1W = palabratop1;
        datosPalabras.Top2 = top2;
        datosPalabras.Top2W = palabratop2;
        datosPalabras.Top3 = top3;
        datosPalabras.Top3W = palabratop3;
        datosPalabras.Top4 = top4;
        datosPalabras.Top4W = palabratop4;
        datosPalabras.Top5 = top5;
        datosPalabras.Top5W = palabratop5;
        datosPalabras.TotalW = TotalPalabras;

        return datosPalabras;
    }

/*--------------------- end algoritmo_dobleSalto end --------------------- */

/* Algoritmo PorPuntoYaparte Más lento más identifica si Inicia on mayuscula se activa la bandera como
posible Parrafo hasta encontrar un \n se espera que termine con un '.' para decir que es parrafo++'
function algoritmo_MayusculaPuntoYaparte(respuesta){
    var Lineas = JSON.parse(respuesta);
    var contador = 0;
    var Posibleparrafo = false;
    var LineaAnterior ='';
    Lineas.forEach(function(linea) {
        if(linea['linea'] == '\n')
        {
            if(Posibleparrafo == true){
                if(PuntoYaparte(LineaAnterior)== true){
                    contador++;
                }
                Posibleparrafo = false;
            }
        }  
        else{
            if(Posibleparrafo == false){
                if(PosibleParrafo(linea['linea'])==true){
                   Posibleparrafo = true;
                 }
            }
        }
        LineaAnterior = linea['linea'];
    });
    return contador;
}
------------------------- Funciones de algoritmo_MayusculaPuntoYaparte---------------
function PuntoYaparte(linea){
    linea = linea.trim();
    var caracter = linea.substring(linea.length-1,linea.length);
    if(caracter == '.')
        return true;
    else
        return false;
}
function PosibleParrafo(linea){
    linea = linea.trim();
    var caracter = linea.substring(0,1);
    return EsMayuscula(caracter);
}
function EsMayuscula(caracter){
    const regxs = {
        "upper": /[A-Z ñÑ]+/
      }
    try {
        if (regxs.upper.test(caracter)) return true;
        else return false;
      }
      catch(error) {
        console.error(error);
      }      
}
-------------------------end Funciones de algoritmo_MayusculaPuntoYaparte end---------------
*/