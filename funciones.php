<?php

switch($_POST['funcion']){
     //Creación de archivo realmente este codigo se puede optimizar y más en el else por que es lo mismo 
      case "SubirArchivoPhP":
        $path = '../mobver-test/Archivos';
            if (file_exists($path)) {
                if (isset($_FILES['fileinput'])) {
                            $tipoarchivo = "archivo";
                            $pathectorio = $path . "/";
                            $file_name = $_FILES['fileinput']['name'];
                            $ext = explode(".", $file_name);
                            $extf = $ext[count($ext) - 1];
                            $destino = $pathectorio . $tipoarchivo . "." . $extf;
                            $file_tmp = $_FILES['fileinput']['tmp_name'];
                            $error = move_uploaded_file(utf8_encode($file_tmp), $destino) or die("ERROR");
                            echo $error;
                }
            }
            else {
              if(mkdir($path, 0777, true)) {
                if (isset($_FILES['fileinput'])) {
                  $tipoarchivo = "archivo";
                  $pathectorio = $path . "/";
                  $file_name = $_FILES['fileinput']['name'];
                  $ext = explode(".", $file_name);
                  $extf = $ext[count($ext) - 1];
                  $destino = $pathectorio . $tipoarchivo . "." . $extf;
                  $file_tmp = $_FILES['fileinput']['tmp_name'];
                  $error = move_uploaded_file(utf8_encode($file_tmp), $destino) or die("ERROR");
                  echo $error;
                }
              }
            }
      break;
    case "LeerArchivoPhP":
       //Codigo reutilizado Copiado https://unipython.com/leer-el-contenido-de-un-archivo-en-php/
       //
        $archivo = fopen("../mobver-test/Archivos/".$_POST['nombre'], "r");
        // Recorremos todas las lineas del archivo
        while(!feof($archivo))
        {
            // Leyendo una linea
            $traer = fgets($archivo);
            // Imprimiendo una linea
            $LineasArchivo = new stdClass();
            $LineasArchivo->linea = $traer;
            $array[] = $LineasArchivo;
        }
        $json = json_encode($array);
        echo $json; //respuesta
        // Cerrando el archivo
        //retorno las lineas
        fclose($archivo);
        break;
    case "CargarTabla":
               //Se encarga de cargar la tabla Información del archivo
               //con la valores de entrada
               $flotante = $_POST['valorflot'];
               $palabralibre = $_POST['txtlibre'];
               $objeto = json_decode($_POST['obj']);
               ?>
                <div class="table-responsive">
                <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Palabra</th>
                      <th>Porcentaje</th>
                      <th>Repetición</th>
                      <th>Aparezco en los Parrafos Inicio 0: (Falta Formato) </th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Palabra</th>
                      <th>Porcentaje</th>
                      <th>Repetición</th>
                      <th>Parrafos</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php
                    foreach ($objeto->dataPalabras as &$word) {
                      $PorSuperar = ($word->repetidos*100)/$objeto->TotalW;
                      if($PorSuperar > $flotante){
                        $palabra = $word->palabra;
                        if($word->palabra == strtoupper($palabralibre)){
                          $palabra = '*'.$palabra; 
                        }
                        $palabraDentroDelporcentaje[] = $word->palabra;
                        ?>
                        <tr>
                          <td><?php echo $palabra ?></td>
                          <td><?php echo $PorSuperar.'%' ?></td>
                          <td><?php echo $word->repetidos ?></td>
                          <td><?php echo $word->parrafo ?></td>
                        </tr>
                        <?php
                      }
                  }
                  $jsonPalabrasP = json_encode($palabraDentroDelporcentaje);
                  ?>
                  </tbody>
                </table>
              </div>
              <input id="jsonPalabrasPermitidas" value='<?=$jsonPalabrasP ?>' name="jsonPalabrasPermitidas" type="hidden" >
              <script src="vendor/datatables/jquery.dataTables.min.js"></script>
              <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
              <script>
                $(document).ready(function() {
                $('#dataTable').DataTable();
                });
              </script>
               <?php
        break;
    case "CargarTablaBigram":
               //Se encarga de cargar la tabla Bigram Creado
               //con la valores de entrada
          
          $objeto = json_decode($_POST['obj']);
          ?>
           <div class="table-responsive">
           <table class="table table-striped table-bordered" id="dataTablebigram" width="100%" cellspacing="0">
             <thead>
               <tr>
                 <th>Palabra Anterior</th>
                 <th>Palabra Posterior</th>
               </tr>
             </thead>
             <tfoot>
               <tr>
                 <th>Palabra Anterior</th>
                 <th>Palabra Posterior</th>
               </tr>
             </tfoot>
             <tbody>
               <?php
               foreach ($objeto->bigram as &$bigrams) {
                 if($bigrams->palabra != "" &&  $bigrams->anterior != ""){
                   ?>
                   <tr>
                      <td><?php echo $bigrams->anterior ?></td>
                     <td><?php echo $bigrams->palabra ?></td>
                   </tr>
                   <?php
                 }
             }
             ?>
             </tbody>
           </table>
         </div>
         <script src="vendor/datatables/jquery.dataTables.min.js"></script>
         <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
         <script>
          $(document).ready(function() {
          $('#dataTablebigram').DataTable();
          });
         </script>
         <?php
        break;
    case "CargarPosiblesSentencias":
         //La Creacion del parrafo
         //tenemos dos objetos
         /*
          $_POST['objpalabras'] contine las palabras que podemos utilizar en base a la probabilidad ingresada
          $_POST['obje2'] contiene un array de objetos cada objeto representa una palabra anterior y una posterior
          lo que seria el bigram

          cada que entra al foreach->contienePalabrasQuesePuedenuTILIZAR 
          se manda a nuestra funcion CrearSentencia 
          esta se encarga de buscar la palabra entre todo nuestro diccionario Bigram
          buscando la palabraQuesePuedenuTILIZAR con una palabra posterior del arraydeobjetos de Bigram
          finalmente se va  creando un array de la palabra que se puedeutilizar y que existe en 
          arraydeobjetos de Bigram por lo tanto.

          Tenemos un array con palabras dentro del porcentaje con una palabra anterior y una posterior
          en base a las encontradas en el documento.

          finalmente no se hace probabilidad que se podria hacer posteriormente...
          pero opte por elegir un indice aleatorio
          que me va a brindar 
          una palabra anterior y una posterior dentro de los datos procesados
          y se va generando el parrafo
          */
          $objeto = json_decode($_POST['objpalabras']);
          $objs = $_POST['obje2'];
          $nuevoParrafo='';
          $linea='';
          foreach ($objeto as &$words) {
            $linea = CrearSentencia($objs,$words);
            $nuevoParrafo = $nuevoParrafo. ' '.$linea;
          }
           echo $nuevoParrafo.'.'.'<br>'; 
        break;
}
function CrearSentencia($objeto,$buscar){
  $objs = json_decode($objeto);
  $opciones = array();
  foreach ($objs->bigram as &$word) {
        if($word->palabra == $buscar)
        {
          $opciones[]=$word->anterior.' '.$word->palabra;
        }
    }
    $palabraelegida = aleatorio($opciones);
  return $opciones[$palabraelegida];
}
function aleatorio($arrayPosible){
  $x = count($arrayPosible);
  $x = $x-1;
  $d=mt_rand(0,$x);
  return $d;
}
?>